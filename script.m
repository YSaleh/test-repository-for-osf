clear all; close all;
load('data.mat') % Load data

%% My old school bar plot:
figure
bar([mean(data.measure(data.cond)),mean(data.measure(~data.cond))]); hold on
errorbar([mean(data.measure(data.cond)),mean(data.measure(~data.cond))],...
    [std(data.measure(data.cond))./sqrt(sum(data.cond)),std(data.measure(~data.cond))./sqrt(sum(~data.cond))],'.k')
xticklabels({'A','B'})
xlabel('Condition')
ylabel('Behavioural measure')
ylim([150 250])



%% Solution with RainCloudPlots
addpath(genpath('fcn'))

cb = cbrewer('qual', 'Set3', 12, 'pchip'); % creates a set of colours

figure
h1 = raincloud_plot(data.measure(data.cond), 'box_on', 1, 'color', cb(1,:), 'alpha', 0.5,...
     'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35,...
     'box_col_match', 1);
h2 = raincloud_plot(data.measure(~data.cond), 'box_on', 1, 'color', cb(4,:), 'alpha', 0.5,...
     'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75,...
     'box_col_match', 1);
legend([h1{1} h2{1}], {'Condition A', 'Condition B'});
xlabel('Behavioural measure','FontSize',15);
box off
ax = gca; ax.YAxis.Visible = 'off'; % removes Y-axis

%% Solution with GRAMM
addpath(genpath('fcn'))

cb = cbrewer('qual', 'Set3', 12, 'pchip'); % creates a set of colours

g = gramm('x',categorical(data.cond,[1 0],{'A','B'}),'y',data.measure,'color',data.cond);
g.stat_violin('half',true,'fill','transparent');
g.geom_jitter('width',0.15,'alpha',0.2);
g.no_legend();
g.set_names('x','Condition','y','Experimental measure');
g.set_text_options('base_size',15)
g.set_color_options('map',cb([1 4],:))

figure
g.draw()

